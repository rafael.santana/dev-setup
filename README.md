# Dev Environment Setup #

## Motivation

Setting up a new developer machine can be a **manual and time-consuming** process.  
The `dev-setup` project aims to **simplify** the process and to **automate** the setup of the following:

* **Ubuntu updates**
* **Java**: Java 8.
* **Automation**: Ansible, Docker and Docker Compose.
* **Javascript**: Node.js, npm, Bower and Gulp.
* **Security**: OpenVPN.

## Requirements

* Ubuntu 16.04

## Install

```sh
$ sudo apt-get install git -y   
$ git clone https://<USERNAME>@git.embraer.com.br/scm/smi/dev-setup.git && cd dev-setup   
$ ./setup.sh
```

## Contribute

Add a new playbook role and run `./test-setup.sh`.  
The `test-setup.sh` script will start a docker container and run the `setup.sh`.
